import glob
import ROOT
import collections
import argparse
import pandas as pd
import Library.Cutflow as cf


Melbourne_pattern=["/atlas/solis/stop_0L/cutflow/4body/CutflowComparison1Lvars.root"]
shef_pattern=["/atlas/solis/ParticlePFlow/Export_21.2.5X/run/test_SRD_1lep/4body_1Lep.root"]
#shef_pattern=["/atlas/solis/ParticlePFlow/Export_21.2.5X/run/stop0L_500_480/4body.root"]

# weightsone=["JetSF","ElecSF","MuonSF","BTagSF","muWeight","XSecWeight","GenWeight","140500","1/10185.5"]
# weightstwo=["AnalysisWeight","LumiWeight","btagweight","jvtweight","ElecWeightReco","MuonWeightReco","pileupweight","140500"]
weightsone=["1==1"]
weightstwo=["1==1"]

dict_vars={
    "NJET":["N_Jets","nj_good"],
    "NBJET":["N_BJets","num_bjets"],
    "MET":["MetTST_met/1000","eT_miss_orig"],
    "NBASELINELEP":["N_SignalLeptons","nbaselineLep"],
    "NSIGNALLEP":["N_SignalLeptons","nEl+nMu"],
    "ISRJET":["ISRJetPt/1000","pT_1lightjet"],
    "DPHIBB":["fabs(trackDphibb)","fabs(dphibb_trk)"],
    "HTSIG":["HtSig","htSig"],
    "DRJ1J2":["DRj1j2","dRj1j2"],
#    "NTRKJET":["NtrackJets","ntrackjets"],
    "NBTRKJET":["NBtrackJets","nbtrackjets"],
    "TRKBJETPT1":["trackb1pt/1000","pT_1btrackjet"],
    # "JETPT1":["Jet_pt[0]/1000","pT_1jet"],
    # "JETPT2":["Jet_pt[1]/1000","pT_2jet"],
    # "JETPT3":["Jet_pt[2]/1000","pT_3jet"],
    # "JETPT4":["Jet_pt[3]/1000","pT_4jet"],
    "PASSMETTRIGGER":["IsMETTrigPassed","passMETtriggers"],
#    "DPHIMIN2":["fabs(JetDPhiMetMin2)","dphimin2"],
#    "DPHIMIN3":["fabs(JetDPhiMetMin3)","dphimin3"],
    "DPHIMIN4":["fabs(AbsDeltaPhiMin4)","dphimin4"],
    "DPHIMIN4MOD":["fabs(AbsDeltaPhiMin4TrackJetModified)","dphimin4_trk"],
    "METTRACK":["MetTrack_met/1000","eT_miss_track"],
    "DPHITRACK":["fabs(DPhiMetTrackMet)","dphi_track_orig"],
    "DPHIBMINMET":["fabs(DPhibminMet)","dphibMetmin"],
    "DPHIISRMET":["fabs(dphiLeadJetMet)","dphi_trk_ISRjMet"],
    "TRK4BJETS":["bTrackjetNTrksFourOrMore==1","passStop0LSRDbtracks==1"]
    }


initial_cut=""
if initial_cut == "":
    initial_cut="1==1"

sample_Melbourne=cf.Sample("Melbourne",Melbourne_pattern,"Stop0L_Nominal",weightsone)
sample_shef=cf.Sample("Sheffield",shef_pattern,"NominalFixed",weightstwo)

mycutflow=cf.cutflow(sample_Melbourne,sample_shef,dict_vars)
mycutflow.AddCut("NBASELINELEP","NBASELINELEP==1."," Baseline lepton veto ")
mycutflow.AddCut("NSIGNALLEP","NSIGNALLEP==1."," Baseline lepton veto ")
mycutflow.AddCut("PASSMETTRIGGER","PASSMETTRIGGER"," Pass MET triggers")
mycutflow.AddCut("MET","MET > 250."," Met>= 250 GeV ")
mycutflow.AddCut("ISRJET","ISRJET> 250."," ISR leading jet >= 250 GeV ")
mycutflow.AddCut("NBJET","NBJET==0"," Number of b-jets =0 ")
mycutflow.AddCut("NBTRKJET","NBTRKJET>0"," Number of track b-jets > 0 ")
mycutflow.AddCut("TRKBJETPT1","TRKBJETPT1>=0 && TRKBJETPT1<30"," Leading b-track jet pT < 30 GeV ")
mycutflow.AddCut("DPHIISRMET","DPHIISRMET>2.4"," dphiISRMET > 2.4 ")
#mycutflow.AddCut("HTSIG","HTSIG>=24."," htSig >= 24")
mycutflow.AddCut("METTRACK","METTRACK > 30"," METTRACK>30")
mycutflow.AddCut("DPHITRACK","DPHITRACK> 0 && DPHITRACK< TMath::Pi()/3"," DPHITRACK < TMath::Pi()/3  ")
mycutflow.AddCut("DPHIMIN4","DPHIMIN4 > 0.4 && DPHIMIN4 < 3.2 "," dphi_min_4 > 0.4 ")
#mycutflow.AddCut("DPHIMIN4MOD","DPHIMIN4MOD > 0.4 && DPHIMIN4MOD < 3.2 "," dphi_min_4mod > 0.4 ")
mycutflow.AddCut("DPHIBMINMET","DPHIBMINMET > 0 && DPHIBMINMET < 0.6"," dphibminMET < 0.6 ")
mycutflow.AddCut("DRJ1J2","DRJ1J2>=2.4"," DRJ1J2 >= 2.4")
mycutflow.AddCut("DPHIBB","DPHIBB > 0.5"," dphibb > 0.5 ")
mycutflow.AddCut("TRK4BJETS","TRK4BJETS"," b-track jets with more than 3 tracks.")




rawframe=mycutflow.getPanda(initial_cut,True)
print("Printing raw number of events comparison")
print(rawframe)
print("")


dataframe=mycutflow.getPanda(initial_cut)
print("Printing weighted number of events comparison")
print(dataframe)
print("")

#mycutflow.printHistograms("plots","(NBASELINELEP==0)*(PASSMETTRIGGER)*(MET>=250.)*(NJET>=4)*(NBJET>=1)*(NANTIKT12)")
#mycutflow.printHistograms("plots_4body_1lep","(NBASELINELEP==1)*(NSIGNALLEP==1)*(PASSMETTRIGGER)*(MET>=250.)*(ISRJET>=250)*(NBTRKJET>0)")
print("Printing raw number histograms")
print("")

print("Printing the cutflow histogram from Melbourne")
MelbourneCutFlow=mycutflow.cutflowFromHist(sample_Melbourne)
Melbournepanda=pd.DataFrame(data=list(zip(MelbourneCutFlow[0],MelbourneCutFlow[1])),columns=["Cuts","Events"])
print(Melbournepanda)

