import glob
import ROOT
import collections
import argparse
import pandas as pd
import Library.Cutflow as cf
#oregon_pattern=["Oregon_21.2.56_v2/T1100L1.root"]
#oregon_pattern=["/atlas/solis/stop_0L/Oregon_21.2.56_2/run/test0L_METTrigger/data-output.01/mc16_13TeV.389245.MadGraphPythia8EvtGen_A14NNPDF23LO_TT_directTT_1100_1.deriv.DAOD_SUSY1.e5511_a875_r9364_p3575.root"]
oregon_pattern=["/atlas/solis/stop_0L/cutflow/0Lep/T1100L1.root"]
#shef_pattern=["/atlas/solis/data/Export_21.2.56/mc16_13TeV.389245.MadGraphPythia8EvtGen_A14NNPDF23LO_TT_directTT_1100_1.e5511_a875_r9364_p3575_stop0L.root"]
#shef_pattern=["/atlas/solis/ParticlePFlow/Export_21.2.5X/run/test_stop0L_1100_1/mc16_13TeV.389245.MadGraphPythia8EvtGen_A14NNPDF23LO_TT_directTT_1100_1.deriv.DAOD_SUSY1.e5511_a875_r9364_p3575.root"]
shef_pattern=["/atlas/solis/ParticlePFlow/Export_21.2.5X/run/stop0L_1100_1_Alt/mc16_13TeV.436147.MGPy8EG_A14N23LO_TT_directTT_1100_1.deriv.DAOD_SUSY1.e6985_a875_r9364_p3712.root"]

weightsone=["GenWeight","XSecWeight","BTagSF","JVTSF","ElecSF","MuonSF","PileupWeight","140500","1/10185.5","rTags==9364"]
weightstwo=["AnalysisWeight","LumiWeight","btagweight","jvtweight","ElecWeightReco","MuonWeightReco","pileupweight","140500","1==1","1==1"]

dict_vars={
    "RTAG":["rTags==9364","1==1"],
    "NJET":["NJets","nj_good"],
    "NBJET":["NBJets","num_bjets"],
    "MET":["Met","eT_miss_orig"],
    "NBASELINEEL":["NBaselineElectrons","nbaselineLep"],
    "NBASELINEMU":["NBaselineMuons","nbaselineLep"],
    "NBASELINELEP":["NBaselineMuons+NBaselineElectrons","nbaselineLep"],
    "NSIGNALMU":["NSignalMuons","nMu"],
    "NSIGNALEL":["NSignalElectrons","nEl"],
    "NSIGNALLEP":["NSignalElectrons+NSignalMuons","nEl+nMu"],
    "JETPT1":["JetPt[0]","pT_1jet"],
    "JETPT2":["JetPt[1]","pT_2jet"],
    "JETPT3":["JetPt[2]","pT_3jet"],
    "JETPT4":["JetPt[3]","pT_4jet"],
    "JETETA1":["JetEta[0]","eta_1jet"],
    "JETETA2":["JetEta[1]","eta_2jet"],
    "JETETA3":["JetEta[2]","eta_3jet"],
    "JETETA4":["JetEta[3]","eta_4jet"],
    "JETPHI1":["JetPhi[0]","phi_1jet"],
    "JETPHI2":["JetPhi[1]","phi_2jet"],
    "JETPHI3":["JetPhi[2]","phi_3jet"],
    "JETPHI4":["JetPhi[3]","phi_4jet"],
    "PASSMETTRIGGER":["PassMetTrigger==1","passMETtriggers"],
    "DPHIMIN2":["JetDPhiMetMin2","dphimin2"],
    "DPHIMIN3":["JetDPhiMetMin3","dphimin3"],
    "DPHIMIN4":["JetDPhiMetMin4","dphimin4"],
    "MTLEPMET":["MtLepMet","MT_orig"],
    "MTBMIN":["MtBMin","MTbmin_orig"],
    "RCKT8PT1":["AntiKt8Pt[0]","pT_1fatjet_kt8"],
    "RCKT8PT2":["AntiKt8Pt[1]","pT_2fatjet_kt8"],
    "NANTIKT12":["NAntiKt12>=2","m_1fatjet_kt12>0. && m_2fatjet_kt12>0."],
    "RCKT12PT1":["AntiKt12Pt[0]","pT_1fatjet_kt12"],
    "RCKT12PT2":["AntiKt12Pt[1]","pT_2fatjet_kt12"],
    "RCKT8M1":["AntiKt8M[0]","m_1fatjet_kt8"],
    "RCKT8M2":["AntiKt8M[1]","m_2fatjet_kt8"],
    "RCKT12M1":["AntiKt12M[0]","m_1fatjet_kt12"],
    "RCKT12M2":["AntiKt12M[1]","m_2fatjet_kt12"],
    "PASSTAUVETO":["MtTauCand < 0.0 || MtTauCand > 1000","passtauveto"],
    "MT2":["MT2Chi2","MT2Chi2"],
    "METTRACK":["MetTrack","eT_miss_track"],
    "DPHITRACK":["DPhiMetTrackMet","dphi_track_orig"],
    #"BJetsPt","pT_1bjet"],
    "DRBB":["DRBB","dRb1b2_weight"],
    }


initial_cut="RTAG"
#initial_cut=""
if initial_cut == "":
    initial_cut="1==1"

sample_oregon=cf.Sample("Oregon",oregon_pattern,"stop_0Lep",weightsone,"CutFlowHist_SRA",["RunNumber"])
sample_shef=cf.Sample("Sheffield",shef_pattern,"NominalFixed",weightstwo,"",["RunNumber"])

mycutflow=cf.cutflow(sample_oregon,sample_shef,dict_vars)

#mycutflow.AddCut("NBASELINEMU","NBASELINEMU==0."," Baseline muons veto ")
mycutflow.AddCut("NBASELINELEP","NBASELINELEP==0."," Baseline lepton veto ")
mycutflow.AddCut("PASSMETTRIGGER","PASSMETTRIGGER"," Pass MET triggers")
mycutflow.AddCut("MET","MET>=250."," MET >= 250 GeV ")
#mycutflow.AddCut("NBASELINEEL","NBASELINEEL==0."," Baseline electrons veto ")
mycutflow.AddCut("NJET","NJET>=4"," Number of good jets >= 4 ")
mycutflow.AddCut("NBJET","NBJET>=1"," Number of b-jets >= 1 ")
mycutflow.AddCut("JETPT2","JETPT2>=80"," p_{T}^{2} >= 80 GeV")
mycutflow.AddCut("JETPT4","JETPT4>=40"," p_{T}^{4} >= 40 GeV")
mycutflow.AddCut("DPHIMIN2","DPHIMIN2>0.4"," dphimin2 > 0.4")
mycutflow.AddCut("METTRACK","METTRACK>30"," Track MET larger than 30 GeV.")
mycutflow.AddCut("DPHITRACK","DPHITRACK<TMath::Pi()/3"," Track MET close to MET")
mycutflow.AddCut("DPHIMIN3","DPHIMIN3>0.4"," dphimin3 > 0.4")
mycutflow.AddCut("NBJET","NBJET>=2"," Number of b-jets >= 2 ")
mycutflow.AddCut("MTBMIN","MTBMIN>=200"," MTbmin >= 200 GeV")
mycutflow.AddCut("DRBB","DRBB>=1"," DRBB > 1")
mycutflow.AddCut("PASSTAUVETO","PASSTAUVETO"," Pass tau veto")
mycutflow.AddCut("NANTIKT12","NANTIKT12"," N antikt2 jets >= 2")
mycutflow.AddCut("RCKT12M1","RCKT12M1>120"," AntiKt12M[0]>120 GeV")
mycutflow.AddCut("RCKT12M2","RCKT12M2>120"," AntiKt12M[1]>120 GeV")
mycutflow.AddCut("RCKT8M1","RCKT8M1>60"," AntiKt8M[0]>60 GeV")
mycutflow.AddCut("MET","MET>=400"," MET >=400 GeV")
mycutflow.AddCut("MT2","MT2>=400"," MT2Chi2 >=400 GeV")

rawframe=mycutflow.getPanda(initial_cut,True)
print("Printing raw number of events comparison")
print(rawframe)
print("")


dataframe=mycutflow.getPanda(initial_cut)
print("Printing weighted number of events comparison")
print(dataframe)
print("")

#mycutflow.printHistograms("plots","(NBASELINELEP==0)*(PASSMETTRIGGER)*(MET>=250.)*(NJET>=4)*(NBJET>=1)*(NANTIKT12)",list(zip(weightsone,weightstwo)))
#mycutflow.printHistograms("plots","(NBASELINELEP==0)*(PASSMETTRIGGER)*(MET>=250.)*(NJET>=4)*(NBJET>=1)",list(zip(weightsone,weightstwo)))
print("Printing raw number histograms")
print("")

print("Printing the cutflow histogram from Oregon")
OregonCutFlow=mycutflow.cutflowFromHist(sample_oregon)
oregonpanda=pd.DataFrame(data=list(zip(OregonCutFlow[0],OregonCutFlow[1])),columns=["Cuts","Events"])
print(oregonpanda)


mycutflow.compareEventNumber([2010],"(NBASELINELEP==0)*(PASSMETTRIGGER)*(MET>=250.)*(NJET>=4)*(NBJET>=1)")

