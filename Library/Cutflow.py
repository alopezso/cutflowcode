import glob
import ROOT
import collections
import argparse
import pandas as pd

ROOT.gROOT.SetBatch(True)

class Variable:
    def __init__(self,common_name,aliases={}):
        self.common=common_name
        self.alias=aliases

class Sample:
    def __init__(self,name,patterns=[],treename="",weights=[],histogram="",eventMatch=[]):
        self.name=name
        self.files=[]
        for pattern in patterns: 
            self.files.extend([i for i in glob.glob(pattern)])
        self.treename=treename
        self.weights=weights
        self.histogram=histogram
        self.eventMatchVars=eventMatch


class cutflow:
    def __init__(self,sampleone,sampletwo=None,dictionary=None):
        self.cutflow=collections.OrderedDict()
        self.sample_one=sampleone
        self.sample_two=sampletwo
        self.chainone=self.getChain(sampleone)
        self.chaintwo=self.getChain(sampletwo)
        self.dictionary=dictionary

    def compareEventNumber(self,eventId=[],selection=""):
        selection=selection.replace("&&",')*(').replace(')*(',' ').replace('(','').replace(')','')
        select_vector=selection.split(" ")
        selOne=[]
        selTwo=[]
        
        for sel in select_vector:
            variable=sel.split('>')[0].split('=')[0].split('<')[0]
            selOne.append(sel.replace(variable,self.dictionary[variable][0]))
            selTwo.append(sel.replace(variable,self.dictionary[variable][1]))

        runOne=[ ("%s==%s"%(x,y)) for x,y in list(zip(self.sample_one.eventMatchVars,eventId)) ]
        runTwo=[ ("%s==%s"%(x,y)) for x,y in list(zip(self.sample_two.eventMatchVars,eventId)) ]
        selectionOne='('+' && '.join(runOne)+')*('.join(selOne)+')'
        selectionTwo='('+' && '.join(runOne)+')*('.join(selTwo)+')'

        self.chainone.Draw(">>elistone",selectionOne);
        self.chainone.Draw(">>elisttwo",selectionOne);
        elistone= ROOT.gDirectory.Get("elistone");
        elisttwo= ROOT.gDirectory.Get("elisttwo");
        self.chainone.SetEntryList(elistone)
        self.chaintwo.SetEntryList(elisttwo)
        
        valuesone=[]
        valuestwo=[]

        labels_x=[]
        treeNum=0    
        for entry in elistone.GetEntries():
            entrytree = elistone.GetEntryAndTree(entry,treeNum)
            entrychain=treeEntry+self.chainone.GetTreeOffset()[treeNum]
            self.chainone.GetEntry(entrychain)
            for var in self.dictionary.items():
                valuesone.append(getattr(self.chainone,var[1][0]))
                labels_x.append(var[0])

            for weight_one,weight_two in weights:
                valuesone.append(getattr(self.chainone,weight_one))
                labels_x.append(weight_one)
        for entry in elisttwo.GetEntries():
            entrytree = elisttwo.GetEntryAndTree(entry,treeNum)
            entrychain=treeEntry+self.chaintwo.GetTreeOffset()[treeNum]
            self.chaintwo.GetEntry(entrychain)
            for var in self.dictionary.items():
                valuestwo.append(getattr(self.chaintwo,var[1][1]))
            for weight_two,weight_two in weights:
                valuestwo.append(getattr(self.chaintwo,weight_two))
                                 
        df=pd.DataFrame(data=list(zip(labels_x,valuesone,valuestwo)),columns=[self.sample_one.name,self.sample_two.name])
        return df
                                 

    def cutflowFromHist(self,sample):
        labels=[]
        values=[]
        if sample.histogram != "":
            histo=None

            for infile in sample.files:
                rootfile=ROOT.TFile(infile,"read")
                print("Retrieving histogram %s from file %s"%(sample.histogram,infile))                
                if histo == None:
                    histo=rootfile.Get(sample.histogram)
                    histo.SetDirectory(0)
                else:
                    histo.Add(rootfile.Get(sample.histogram))
                rootfile.Close()

            print(histo.GetNbinsX())
            for bin in range(1,histo.GetNbinsX()+1):
                labels.append(histo.GetXaxis().GetBinLabel(bin))
                values.append(histo.GetBinContent(bin))
        return [labels,values]
            
    def AddCut(self,variable,cut,label):
        while variable in self.cutflow.keys():
            variable+=" "
##            print("Cut already setup on variable %s.Overwriting it."%variable)
        self.cutflow[variable]=[cut,label]

            
    def printHistograms(self,outFolder,selection="",weights=[]):

        canv=ROOT.TCanvas("canvas","canvas")
        selection=selection.replace("&&",')*(').replace(')*(',' ').replace('(','').replace(')','')
        select_vector=selection.split(" ")
        selOne=[]
        selTwo=[]
        
        for sel in select_vector:
            variable=sel.split('>')[0].split('=')[0].split('<')[0]
            selOne.append(sel.replace(variable,self.dictionary[variable][0]))
            selTwo.append(sel.replace(variable,self.dictionary[variable][1]))
        selectionOne='('+')*('.join(selOne)+')'
        selectionTwo='('+')*('.join(selTwo)+')'

        count=0
        for weight_one,weight_two in weights:
            if "/"  in weight_one:
                self.dictionary["weights_%d"%count]=[weight_one,weight_two]
                count+=1
            else:
                self.dictionary[weight_one]=[weight_one,weight_two]

        for var in self.dictionary.items():
            canv.Clear()
            canv.cd()
            self.chainone.Draw(var[1][0]+" >> histoOne"+var[0],selectionOne)
            self.chaintwo.Draw(var[1][1]+" >> histoTwo"+var[0],selectionTwo,"SAME")
            histoOne=ROOT.gDirectory.Get('histoOne'+var[0])
            histoTwo=ROOT.gDirectory.Get('histoTwo'+var[0])
            canv.Clear()
            histoOne.SetLineColor(ROOT.kBlue)
            histoTwo.SetLineColor(ROOT.kRed)
            histoOne.SetLineWidth(2)
            histoTwo.SetLineWidth(2)
            histoOne.SetStats(0)
            histoTwo.SetStats(0)
            histoOne.DrawNormalized()
            histoTwo.DrawNormalized("SAME")
            canv.Update()
            leg=ROOT.TLegend(0.6,0.6,0.85,0.85)
            leg.SetBorderSize(0)
            leg.AddEntry(histoOne,self.sample_one.name,"lp")
            leg.AddEntry(histoTwo,self.sample_two.name,"lp")
            leg.Draw("SAME")
            canv.SaveAs(outFolder+"/"+var[0]+".pdf","RECREATE")
            del histoOne
            del histoTwo

    def getPanda(self,initial_cut="1==1",rawEvents=False):
        cuts_one=initial_cut
        cuts_two=initial_cut
        for var in self.dictionary.items():
            cuts_one=cuts_one.replace(var[0],var[1][0])
            cuts_two=cuts_two.replace(var[0],var[1][1])

        selone,seltwo="",""
        if rawEvents:
            selone="("+cuts_one+")"
            seltwo="("+cuts_two+")"
        else:
            selone="("+")*(".join(self.sample_one.weights)+")*("+cuts_one+")"
            seltwo="("+")*(".join(self.sample_two.weights)+")*("+cuts_two+")"
        labels_x=[]
        labels_y=["Cuts",self.sample_one.name,self.sample_two.name]
        events_one=[]
        events_two=[]
        self.chainone.Draw(" 1 >> histone",selone,"goff")
        self.chaintwo.Draw(" 1 >> histtwo",seltwo,"goff")
        histone=ROOT.gDirectory.Get("histone")
        histtwo=ROOT.gDirectory.Get("histtwo")
        events_one.append(histone.Integral())
        events_two.append(histtwo.Integral())
        labels_x.append("Initial Cut")
        for cut in self.cutflow.items():
            selone+="*("+cut[1][0].replace(cut[0].split(' ')[0],self.dictionary[cut[0].split(' ')[0]][0])+")"
#            if not "[" in  cut: 
            seltwo+="*("+cut[1][0].replace(cut[0].split(' ')[0],self.dictionary[cut[0].split(' ')[0]][1])+")"
            # else:
            #     ## This in case that we are trying to access a vector information like  JetPt[1] etc. 
            #     seltwo+="*("+cut[1][0].replace(cut[0].split("[")[0],self.dictionary[cut[0].split("[")[0]])+")"
            labels_x.append(cut[1][1])
            self.chainone.Draw(" 1 >> histone",selone,"goff")
            self.chaintwo.Draw(" 1 >> histtwo",seltwo,"goff")
            histone=ROOT.gDirectory.Get("histone")
            histtwo=ROOT.gDirectory.Get("histtwo")
            events_one.append(histone.Integral())
            events_two.append(histtwo.Integral())
        df=pd.DataFrame(data=list(zip(labels_x,events_one,events_two)),columns=labels_y)
        return df


    def getChain(self,sample):
        
        chain = ROOT.TChain(sample.treename,sample.treename)
        for infile in sample.files:
            chain.Add(infile)
            print(" INFO : Loading file %s from sample %s"%(infile,sample.name))
            
        return chain
