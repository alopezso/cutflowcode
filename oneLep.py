import glob
import ROOT
import collections
import argparse
import pandas as pd
import Library.Cutflow as cf

oregon_pattern=["/atlas/solis/stop_0L/Oregon_21.2.56_2/run/1lepton/Wjets.root"]
shef_pattern=["/atlas/solis/ParticlePFlow/Export_21.2.5X/run/Wjets_1lep/mc16_13TeV.364182.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV500_1000.deriv.DAOD_SUSY1.e5340_s3126_r9364_p3703.root"]
#oregon_pattern=["/atlas/solis/stop_0L/Oregon_21.2.56_2/run/test_1lep/data-output.01/ttbar.root.root"]
#shef_pattern=["/atlas/solis/ParticlePFlow/Export_21.2.5X/run/ttbar_1lep/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY1.e6337_s3126_r10201_p3703.root"]

weightsone=["JVTSF","ElecSF","MuonSF","BTagSF","PileupWeight","XSecWeight","GenWeight","140500","1/(5.4033625*TMath::Power(10,8))"]
weightstwo=["AnalysisWeight","LumiWeight","btagweight","jvtweight","ElecWeightReco","MuonWeightReco","pileupweight","140500"]

dict_vars={
    "NJET":["NJets","nj_good"],
    "NBJET":["NBJets","num_bjets"],
    "MET":["Met","eT_miss_orig"],
    "NBASELINEEL":["NBaselineElectrons","nbaselineLep"],
    "NBASELINEMU":["NBaselineMuons","nbaselineLep"],
    "NBASELINELEP":["NBaselineMuons+NBaselineElectrons","nbaselineLep"],
    "NSIGNALMU":["NSignalMuons","nMu"],
    "NSIGNALEL":["NSignalElectrons","nEl"],
    "NSIGNALLEP":["NSignalElectrons+NSignalMuons","nEl+nMu"],
    "LEPPT1":["pT_1lep","pT_1lep"],
    "JETPT1":["JetPt[0]","pT_1jet"],
    "JETPT2":["JetPt[1]","pT_2jet"],
    "JETPT3":["JetPt[2]","pT_3jet"],
    "JETPT4":["JetPt[3]","pT_4jet"],
    "JETETA1":["JetEta[0]","eta_1jet"],
    "JETETA2":["JetEta[1]","eta_2jet"],
    "JETETA3":["JetEta[2]","eta_3jet"],
    "JETETA4":["JetEta[3]","eta_4jet"],
    "JETPHI1":["JetPhi[0]","phi_1jet"],
    "JETPHI2":["JetPhi[1]","phi_2jet"],
    "JETPHI3":["JetPhi[2]","phi_3jet"],
    "JETPHI4":["JetPhi[3]","phi_4jet"],
    "PASSMETTRIGGER":["PassMetTrigger==1","passMETtriggers"],
    "DPHIMIN2":["JetDPhiMetMin2","dphimin2"],
    "DPHIMIN3":["JetDPhiMetMin3","dphimin3"],
    "DPHIMIN4":["JetDPhiMetMin4","dphimin4"],
    "MTLEPMET":["MtLepMet","MT_orig"],
#    "DRLBMIN":["","DRlbmin"],
    
    "MTBMIN":["MtBMin","MTbmin_orig"],
    "RCKT8PT1":["AntiKt8Pt[0]","pT_1fatjet_kt8"],
    "RCKT8PT2":["AntiKt8Pt[1]","pT_2fatjet_kt8"],
    "NANTIKT12":["NAntiKt12","m_1fatjet_kt12>0. && m_2fatjet_kt12>0."],
    "RCKT12PT1":["AntiKt12Pt[0]","pT_1fatjet_kt12"],
    "RCKT12PT2":["AntiKt12Pt[1]","pT_2fatjet_kt12"],
    "RCKT8M1":["AntiKt8M[0]","m_1fatjet_kt8"],
    "RCKT8M2":["AntiKt8M[1]","m_2fatjet_kt8"],
    "RCKT12M1":["AntiKt12M[0]","m_1fatjet_kt12"],
    "RCKT12M2":["AntiKt12M[1]","m_2fatjet_kt12"],
    "PASSTAUVETO":["MtTauCand < 0.0 || MtTauCand > 1000","passtauveto"],
    "MT2":["MT2Chi2","MT2Chi2"],
    "METTRACK":["MetTrack","eT_miss_track"],
    "DPHITRACK":["DPhiMetTrackMet","dphi_track_orig"],
    #"BJetsPt","pT_1bjet"],
    "DRBB":["DRBB","dRb1b2_weights"],
    }


initial_cut=""
if initial_cut == "":
    initial_cut="1==1"

sample_oregon=cf.Sample("Oregon",oregon_pattern,"stop_1Lep",weightsone)
sample_shef=cf.Sample("Sheffield",shef_pattern,"NominalFixed",weightstwo)

mycutflow=cf.cutflow(sample_oregon,sample_shef,dict_vars)

mycutflow.AddCut("NBASELINELEP","NBASELINELEP==1."," One baseline lepton  ")
mycutflow.AddCut("NSIGNALLEP","NSIGNALLEP==1."," One signal lepton ")
mycutflow.AddCut("PASSMETTRIGGER","PASSMETTRIGGER"," Pass MET triggers")
mycutflow.AddCut("MET","MET>=250."," MET >= 250 GeV ")
mycutflow.AddCut("LEPPT1","LEPPT1>=20"," p_{T} lepton >= 20 GeV ")
mycutflow.AddCut("NJET","NJET>=3"," Number of good jets >= 3 ")
mycutflow.AddCut("NBJET","NBJET>=2"," Number of b-jets >= 2 ")
mycutflow.AddCut("JETPT2","JETPT2>=80"," p_{T}^{2} >= 80 GeV")
mycutflow.AddCut("JETPT4","JETPT4>=40"," p_{T}^{4} >= 40 GeV")
mycutflow.AddCut("DPHIMIN2","DPHIMIN2>0.4"," dphimin2 > 0.4")
mycutflow.AddCut("METTRACK","METTRACK>30"," Track MET larger than 30 GeV.")
mycutflow.AddCut("DPHITRACK","DPHITRACK<TMath::Pi()/3"," Track MET close to MET")
mycutflow.AddCut("DPHIMIN3","DPHIMIN3>0.4"," dphimin3 > 0.4")
mycutflow.AddCut("MTBMIN","MTBMIN>=100"," MTbmin >= 100 GeV")
mycutflow.AddCut("MTLEPMET","MTLEPMET>=30 && MTLEPMET<100 "," MT(lepton,MET) in [30,100] GeV")
#mycutflow.AddCut("DRLBMIN","DRLBMIN<1.5"," dR(lepton,b-jet)min < 1.5")
mycutflow.AddCut("RCKT12M1","RCKT12M1>120"," AntiKt12M[0]>120 GeV")


rawframe=mycutflow.getPanda(initial_cut,True)
print("Printing raw number of events comparison")
print(rawframe)
print("")


dataframe=mycutflow.getPanda(initial_cut)
print("Printing weighted number of events comparison")
print(dataframe)
print("")

#mycutflow.printHistograms("plots","(NBASELINELEP==0)*(PASSMETTRIGGER)*(MET>=250.)*(NJET>=4)*(NBJET>=1)*(NANTIKT12)")
mycutflow.printHistograms("plots_1lep","(NBASELINELEP==1)*(NSIGNALLEP==1)*(PASSMETTRIGGER)*(MET>=250.)*(NJET>=3)*(NBJET>=2)")
print("Printing raw number histograms")
print("")

# print("Printing the cutflow histogram from Oregon")
# OregonCutFlow=mycutflow.cutflowFromHist(sample_oregon)
# oregonpanda=pd.DataFrame(data=list(zip(OregonCutFlow[0],OregonCutFlow[1])),columns=["Cuts","Events"])
# print(oregonpanda)

