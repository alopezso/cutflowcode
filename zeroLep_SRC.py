import glob
import ROOT
import collections
import argparse
import pandas as pd
import Library.Cutflow as cf
oregon_pattern=["/atlas/solis/stop_0L/Oregon_21.2.56_2/run/test_0lepSRC/data-output.01/ttbar.root.root"]
shef_pattern=["/atlas/solis/ParticlePFlow/Export_21.2.5X/run/stop0L_SRC/mc16_13TeV.387231.MGPy8EG_A14N23LO_TT_directTT_500_327_MET100.deriv.DAOD_SUSY1.e5666_a875_r9364_p3575.root"]

weightsone=["JVTSF","ElecSF","MuonSF","BTagSF","PileupWeight","XSecWeight","GenWeight","140500","1/217539.23"]
weightstwo=["AnalysisWeight","LumiWeight","btagweight","jvtweight","ElecWeightReco","MuonWeightReco","pileupweight","140500"]

dict_vars={
    "NJET":["NJets","nj_good"],
    "NBJET":["NBJets","num_bjets"],
    "MET":["Met","eT_miss_orig"],
    "NBASELINEEL":["NBaselineElectrons","nbaselineLep"],
    "NBASELINEMU":["NBaselineMuons","nbaselineLep"],
    "NBASELINELEP":["NBaselineMuons+NBaselineElectrons","nbaselineLep"],
    "NSIGNALMU":["NSignalMuons","nMu"],
    "NSIGNALEL":["NSignalElectrons","nEl"],
    "NSIGNALLEP":["NSignalElectrons+NSignalMuons","nEl+nMu"],
    "JETPT1":["JetPt[0]","pT_1jet"],
    "JETPT2":["JetPt[1]","pT_2jet"],
    "JETPT3":["JetPt[2]","pT_3jet"],
    "JETPT4":["JetPt[3]","pT_4jet"],
    "JETETA1":["JetEta[0]","eta_1jet"],
    "JETETA2":["JetEta[1]","eta_2jet"],
    "JETETA3":["JetEta[2]","eta_3jet"],
    "JETETA4":["JetEta[3]","eta_4jet"],
    "JETPHI1":["JetPhi[0]","phi_1jet"],
    "JETPHI2":["JetPhi[1]","phi_2jet"],
    "JETPHI3":["JetPhi[2]","phi_3jet"],
    "JETPHI4":["JetPhi[3]","phi_4jet"],
    "PASSMETTRIGGER":["PassMetTrigger==1","passMETtriggers"],
    "DPHIMIN2":["JetDPhiMetMin2","dphimin2"],
    "DPHIMIN3":["JetDPhiMetMin3","dphimin3"],
    "DPHIMIN4":["JetDPhiMetMin4","dphimin4"],
    "MTLEPMET":["MtLepMet","MT_orig"],
    "MTBMIN":["MtBMin","MTbmin_orig"],
    "RCKT8PT1":["AntiKt8Pt[0]","pT_1fatjet_kt8"],
    "RCKT8PT2":["AntiKt8Pt[1]","pT_2fatjet_kt8"],
    "NANTIKT12":["NAntiKt12>=2","m_1fatjet_kt12>0. && m_2fatjet_kt12>0."],
    "RCKT12PT1":["AntiKt12Pt[0]","pT_1fatjet_kt12"],
    "RCKT12PT2":["AntiKt12Pt[1]","pT_2fatjet_kt12"],
    "RCKT8M1":["AntiKt8M[0]","m_1fatjet_kt8"],
    "RCKT8M2":["AntiKt8M[1]","m_2fatjet_kt8"],
    "RCKT12M1":["AntiKt12M[0]","m_1fatjet_kt12"],
    "RCKT12M2":["AntiKt12M[1]","m_2fatjet_kt12"],
    "PASSTAUVETO":["MtTauCand < 0.0 || MtTauCand > 1000","passtauveto"],
    "MT2":["MT2Chi2","MT2Chi2"],
    "METTRACK":["MetTrack","eT_miss_track"],
    "DPHITRACK":["DPhiMetTrackMet","dphi_track_orig"],
    #"BJetsPt","pT_1bjet"],
    "DRBB":["DRBB","dRb1b2_weights"],
    "NBV":["CA_NbV","NbV"],
    "NJV":["CA_NjV","NjV"],
    "PTISR":["CA_PTISR","PTISR"],
    "MS":["CA_MS","MS"],
    "DPHIISR":["CA_dphiISRI","dphiISRI"],
    "PT4S":["CA_pTjV4/1000","pTjV4"],
    "RISR":["CA_RISR","RISR"],
    "PT1B":["CA_pTbV1/1000","pTbV1"],
    "METSIG":["MetSig","metsig"],
    }


initial_cut=""
if initial_cut == "":
    initial_cut="1==1"

sample_oregon=cf.Sample("Oregon",oregon_pattern,"stop_0Lep",weightsone,"CutFlowHist_SRA")
sample_shef=cf.Sample("Sheffield",shef_pattern,"NominalFixed",weightstwo)

mycutflow=cf.cutflow(sample_oregon,sample_shef,dict_vars)

#mycutflow.AddCut("NBASELINEMU","NBASELINEMU==0."," Baseline muons veto ")
mycutflow.AddCut("NBASELINELEP","NBASELINELEP==0."," Baseline lepton veto ")
mycutflow.AddCut("PASSMETTRIGGER","PASSMETTRIGGER"," Pass MET triggers")
mycutflow.AddCut("MET","MET>=250."," MET >= 250 GeV ")
#mycutflow.AddCut("NBASELINEEL","NBASELINEEL==0."," Baseline electrons veto ")
mycutflow.AddCut("NJET","NJET>=4"," Number of good jets >= 4 ")
mycutflow.AddCut("NBJET","NBJET>=1"," Number of b-jets >= 1 ")
mycutflow.AddCut("JETPT2","JETPT2>=80"," p_{T}^{2} >= 80 GeV")
mycutflow.AddCut("JETPT4","JETPT4>=40"," p_{T}^{4} >= 40 GeV")
mycutflow.AddCut("DPHIMIN2","DPHIMIN2>0.4"," dphimin2 > 0.4")
mycutflow.AddCut("METTRACK","METTRACK>30"," Track MET larger than 30 GeV.")
mycutflow.AddCut("DPHITRACK","DPHITRACK<TMath::Pi()/3"," Track MET close to MET")
mycutflow.AddCut("NBJET","NBJET>=2"," Number of b-jets >= 2 ")
mycutflow.AddCut("NBV","NBV>=1"," Number of b-jets in the s-hemisphere >= 1 ")
mycutflow.AddCut("NJV","NJV>=5"," Number of jets in the s-hemisphere >= 1 ")
mycutflow.AddCut("PT1B","PT1B>=40"," Leading b-jet pT in the s-hemisphere >= 40 GeV")
mycutflow.AddCut("PT4S","PT4S>=50"," Fourth leading jet pT in the s-hemisphere >= 50 GeV ")
mycutflow.AddCut("DPHIISR","DPHIISR>=3.0"," azimuthal between MET and ISR system >=3.00 ")
mycutflow.AddCut("MS","MS>=300"," MS >= 300 GeV ")
mycutflow.AddCut("PTISR","PTISR>=400"," pTISR >= 400 GeV ")


rawframe=mycutflow.getPanda(initial_cut,True)
print("Printing raw number of events comparison")
print(rawframe)
print("")


dataframe=mycutflow.getPanda(initial_cut)
print("Printing weighted number of events comparison")
print(dataframe)
print("")

# #mycutflow.printHistograms("plots","(NBASELINELEP==0)*(PASSMETTRIGGER)*(MET>=250.)*(NJET>=4)*(NBJET>=1)*(NANTIKT12)")
# mycutflow.printHistograms("plots_SRC","(NBASELINELEP==0)*(PASSMETTRIGGER)*(MET>=250.)*(NJET>=4)*(NBJET>=1)")
# print("Printing raw number histograms")
# print("")

# print("Printing the cutflow histogram from Oregon")
# OregonCutFlow=mycutflow.cutflowFromHist(sample_oregon)
# oregonpanda=pd.DataFrame(data=list(zip(OregonCutFlow[0],OregonCutFlow[1])),columns=["Cuts","Events"])
# print(oregonpanda)

