import glob
import ROOT
import collections
import argparse
import pandas as pd

#oregon_pattern=["/atlas/shatlas/NTUP_SUSY/tt0L_Oregon/v21.2.56-1/T1100L1.root"]
#oregon_pattern=["/atlas/solis/stop_0L/OregonCode/run/test/data-output.01//mc16_13TeV.389245.MadGraphPythia8EvtGen_A14NNPDF23LO_TT_directTT_1100_1.deriv.DAOD_SUSY1.e5511_a875_r9364_p3575.root"]
oregon_pattern=["/atlas/solis/stop_0L/OregonCode/run/test/_TargetDir/T1100L1Processed.root"]
shef_pattern=["/atlas/solis/data/Export_21.2.56/mc16_13TeV.389245.MadGraphPythia8EvtGen_A14NNPDF23LO_TT_directTT_1100_1.e5511_a875_r9364_p3575_stop0L.root"]
#shef_pattern=["/atlas/anthony/2019/EOY18_Production_TopTag/mc16_13TeV.389245.MadGraphPythia8EvtGen_A14NNPDF23LO_TT_directTT_1100_1.e5511_a875_r9364_p3575_stop0L.root"]

weightsone=["JVTSF","ElecSF","MuonSF","BTagSF","PileupWeight","XSecWeight","GenWeight","140500","1/10185.5"]
weightstwo=["AnalysisWeight","LumiWeight","btagweight","jvtweight","ElecWeight","MuonWeight","pileupweight","140500"]



dict_vars={
    "NJET":["NJets","nj_good"],
    "NBJET":["NBJets","num_bjets"],
    "MET":["Met","eT_miss_orig"],
    "NBASELINEEL":["NBaselineElectrons","nbaselineLep"],
    "NBASELINEMU":["NBaselineMuons","nbaselineLep"],
    "NBASELINELEP":["NBaselineMuons+NBaselineElectrons","nbaselineLep"],
    "NSIGNALMU":["NSignalMuons","nMu"],
    "NSIGNALEL":["NSignalElectrons","nEl"],
    "NSIGNALLEP":["NSignalElectrons+NSignalMuons","nEl+nMu"],
    "JETPT1":["JetPt[0]","pT_1jet"],
    "JETPT2":["JetPt[1]","pT_2jet"],
    "JETPT3":["JetPt[2]","pT_3jet"],
    "JETPT4":["JetPt[3]","pT_4jet"],
    "PASSMETTRIGGER":["PassMetTrigger","passMETtriggers"],
    "DPHIMIN2":["JetDPhiMetMin2","dphimin2"],
    "DPHIMIN3":["JetDPhiMetMin3","dphimin3"],
    "DPHIMIN4":["JetDPhiMetMin4","dphimin4"],
    "MTLEPMET":["MtLepMet","MT_orig"],
    "MTBMIN":["MtBMin","MTbmin_orig"],
    "RCKT8PT1":["AntiKt8Pt[0]","pT_1fatjet_kt8"],
    "RCKT8PT2":["AntiKt8Pt[1]","pT_2fatjet_kt8"],
    "RCKT12PT1":["AntiKt12Pt[0]","pT_1fatjet_kt8"],
    "RCKT12PT2":["AntiKt12Pt[1]","pT_2fatjet_kt12"],
    "RCKT8M1":["AntiKt8M[0]","m_1fatjet_kt8"],
    "RCKT8M2":["AntiKt8M[1]","m_2fatjet_kt8"],
    "RCKT12M1":["AntiKt12M[0]","m_1fatjet_kt8"],
    "RCKT12M2":["AntiKt12M[1]","m_2fatjet_kt12"],
    "PASSTAUVETO":["MtTauCand< -0.0001","passtauveto"],
    "MT2":["MT2Chi2","MT2Chi2"],
    "METTRACK":["MetTrack","eT_miss_track"],
    "DPHITRACK":["DPhiMetTrackMet","dphi_track_orig"],
    #"BJetsPt","pT_1bjet"],
    "DRBB":["DRBB","dRb1b2"],
    }


initial_cut=""
if initial_cut == "":
    initial_cut="1==1"

class sample:
    def __init__(self,name,patterns=[],treename="",weights=[],histogram=""):
        self.name=name
        self.files=[]
        for pattern in patterns: 
            self.files.extend([i for i in glob.glob(pattern)])
        self.treename=treename
        self.weights=weights
        self.histogram=histogram


class cutflow:
    def __init__(self,sampleone,sampletwo):
        self.cutflow=collections.OrderedDict()
#        self.cutflow={}
        self.sample_one=sampleone
        self.sample_two=sampletwo
        self.chainone=self.getChain(sampleone)
        self.chaintwo=self.getChain(sampletwo)
        self.dictionary=dict_vars

    def cutflowFromHist(self,sample):
        labels=[]
        values=[]
        if sample.histogram != "":
            histo=None

            for infile in sample.files:
                rootfile=ROOT.TFile(infile,"read")
                print("Retrieving histogram %s from file %s"%(sample.histogram,infile))                
                if histo == None:
                    histo=rootfile.Get(sample.histogram)
                    histo.SetDirectory(0)
                else:
                    histo.Add(rootfile.Get(sample.histogram))
                rootfile.Close()

            print(histo.GetNbinsX())
            for bin in range(1,histo.GetNbinsX()+1):
                labels.append(histo.GetXaxis().GetBinLabel(bin))
                values.append(histo.GetBinContent(bin))
        return [labels,values]
            
    def AddCut(self,variable,cut,label):
        while variable in self.cutflow.keys():
            variable+=" "
##            print("Cut already setup on variable %s.Overwriting it."%variable)
        self.cutflow[variable]=[cut,label]

    def getPanda(self,rawEvents=False):
        cuts_one=initial_cut
        cuts_two=initial_cut
        for var in self.dictionary.items():
            cuts_one=cuts_one.replace(var[0],var[1][0])
            cuts_two=cuts_two.replace(var[0],var[1][1])

        selone,seltwo="",""
        if rawEvents:
            selone="("+cuts_one+")"
            seltwo="("+cuts_two+")"
        else:
            selone="("+")*(".join(self.sample_one.weights)+")*("+cuts_one+")"
            seltwo="("+")*(".join(self.sample_two.weights)+")*("+cuts_two+")"
        labels_x=[]
        labels_y=["Cuts",self.sample_one.name,self.sample_two.name]
        events_one=[]
        events_two=[]
        self.chainone.Draw(" 1 >> histone",selone,"goff")
        self.chaintwo.Draw(" 1 >> histtwo",seltwo,"goff")
        histone=ROOT.gDirectory.Get("histone")
        histtwo=ROOT.gDirectory.Get("histtwo")
        events_one.append(histone.Integral())
        events_two.append(histtwo.Integral())
        labels_x.append("Initial Cut")
        for cut in self.cutflow.items():
            selone+="*("+cut[1][0].replace(cut[0].split(' ')[0],self.dictionary[cut[0].split(' ')[0]][0])+")"
#            if not "[" in  cut: 
            seltwo+="*("+cut[1][0].replace(cut[0].split(' ')[0],self.dictionary[cut[0].split(' ')[0]][1])+")"
            # else:
            #     ## This in case that we are trying to access a vector information like  JetPt[1] etc. 
            #     seltwo+="*("+cut[1][0].replace(cut[0].split("[")[0],self.dictionary[cut[0].split("[")[0]])+")"
            labels_x.append(cut[1][1])
            self.chainone.Draw(" 1 >> histone",selone,"goff")
            self.chaintwo.Draw(" 1 >> histtwo",seltwo,"goff")
            histone=ROOT.gDirectory.Get("histone")
            histtwo=ROOT.gDirectory.Get("histtwo")
            events_one.append(histone.Integral())
            events_two.append(histtwo.Integral())
        df=pd.DataFrame(data=list(zip(labels_x,events_one,events_two)),columns=labels_y)
        return df


    def getChain(self,sample):
        
        chain = ROOT.TChain(sample.treename,sample.treename)
        for infile in sample.files:
            chain.Add(infile)
            print(" INFO : Loading file %s from sample %s"%(infile,sample.name))
            
        return chain


sample_oregon=sample("Oregon",oregon_pattern,"stop_0Lep",weightsone,"CutFlowHist_SRA")
sample_shef=sample("Sheffield",shef_pattern,"NominalFixed",weightstwo)


mycutflow=cutflow(sample_oregon,sample_shef)

mycutflow.AddCut("PASSMETTRIGGER","PASSMETTRIGGER"," Pass MET triggers")
mycutflow.AddCut("MET","MET>=250."," MET >= 250 GeV ")
#mycutflow.AddCut("NBASELINEMU","NBASELINEMU==0."," Baseline muons veto ")
mycutflow.AddCut("NBASELINELEP","NBASELINELEP==0."," Baseline lepton veto ")
#mycutflow.AddCut("NBASELINEEL","NBASELINEEL==0."," Baseline electrons veto ")
mycutflow.AddCut("NJET","NJET>=4"," Number of good jets >= 4 ")
mycutflow.AddCut("NBJET","NBJET>=1"," Number of b-jets >= 1 ")
mycutflow.AddCut("JETPT1","JETPT1>=80"," p_{T}^{2} >= 80 GeV")
mycutflow.AddCut("JETPT3","JETPT3>=40"," p_{T}^{4} >= 40 GeV")
mycutflow.AddCut("DPHIMIN2","DPHIMIN2>0.4"," dphimin2 > 0.4")
mycutflow.AddCut("METTRACK","METTRACK>30"," Track MET larger than  0.4")
mycutflow.AddCut("DPHITRACK","DPHITRACK<TMath::Pi()/3"," Track MET close to MET")
mycutflow.AddCut("DPHIMIN3","DPHIMIN3>0.4"," dphimin3 > 0.4")
mycutflow.AddCut("NBJET","NBJET>=2"," Number of b-jets >= 2 ")
mycutflow.AddCut("MTBMIN","MTBMIN>=200"," MTbmin >= 200 GeV")
mycutflow.AddCut("DRBB","DRBB>=1"," DRBB > 1")
mycutflow.AddCut("MT2","MT2>=400"," MT2Chi2 >=400 GeV")
mycutflow.AddCut("PASSTAUVETO","PASSTAUVETO"," Pass tau veto")
mycutflow.AddCut("MET","MET>=400"," MET >=400 GeV")
#mycutflow.AddCut("DRBB","DRBB>=1"," DRBB > 1")
mycutflow.AddCut("RCKT12M1","RCKT12M1>120"," AntiKt12M[0]>120 GeV")
mycutflow.AddCut("RCKT12M2","RCKT12M2>120"," AntiKt12M[1]>120 GeV")
mycutflow.AddCut("RCKT8M1","RCKT8M1>60"," AntiKt8M[0]>60 GeV")

rawframe=mycutflow.getPanda(True)
print("Printing raw number of events comparison")
print(rawframe)
print("")


dataframe=mycutflow.getPanda()
print("Printing weighted number of events comparison")
print(dataframe)
print("")

print("Printing the cutflow histogram from Oregon")
OregonCutFlow=mycutflow.cutflowFromHist(sample_oregon)
oregonpanda=pd.DataFrame(data=list(zip(OregonCutFlow[0],OregonCutFlow[1])),columns=["Cuts","Events"])
print(oregonpanda)

